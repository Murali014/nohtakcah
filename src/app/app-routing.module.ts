import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotesComponentComponent } from './notes-component/notes-component.component';
import { DetailsComponentComponent } from './details-component/details-component.component';
import { ReminderComponentComponent } from './reminder-component/reminder-component.component';

const routes: Routes = [
  { path: 'notes', component: NotesComponentComponent,data: { title: 'My Calendar' } },
  { path: 'reminders', component: ReminderComponentComponent },
  { path: 'details', component: DetailsComponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
