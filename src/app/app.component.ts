import { Component, Input, Output } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Nohtakcah';
  showFiller = false;
  heading_Title = "NOTES";
  menu_List = []

  constructor(private router: Router){

  }
  ngOnInit() {
    //  MENU LIST
    this.menu_List.push({ name: "NOTES", url: "/notes"});
    this.menu_List.push({ name: "REMINDERS", url: "/reminders"});
    this.menu_List.push({ name: "DETAILS", url: "/details"});
    this.heading_Title = (window.location.pathname).slice(1).toUpperCase();
  }

  // Redirects to menu components
  changeRoute(menu_Item: string){
    this.router.navigate([menu_Item.toLowerCase()]);
    this.heading_Title = menu_Item.toUpperCase();

  }
}
